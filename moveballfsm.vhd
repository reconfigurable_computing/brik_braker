library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity moveballfsm is
	port
	(
		CLK		: in std_logic;
		RST		: in std_logic;
		DROP_BALL: in std_logic;
		
		ball_x : out natural;
		ball_y : out natural
	
	);
end entity moveballfsm;

architecture behav of moveballfsm is

type state_type is (reset, waiting_for_drop, ball_drop, move_left_up, move_left_down, move_right_down, move_right_up);
signal cstate, nstate : state_type := reset;

signal count, ncount : natural := 0;
signal x_coord, nx_coord : natural := 320;
signal y_coord, ny_coord : natural := 240;

begin
	sync_proc : process (CLK,RST)
	begin	
		if rising_edge(CLK) then
			if RST = '1' then
				count <= 0;
				cstate <= reset;
				x_coord <= 320;
				y_coord <= 240;
			else
				count <= ncount;
				cstate <= nstate;
				x_coord <= nx_coord;
				y_coord <= ny_coord;
				ball_x <= x_coord;
				ball_y <= y_coord;
			end if;
		end if;
	end process;
	
	async_proc : process (count, cstate, x_coord, y_coord, count)
	variable x_var, y_var : integer := 1;
	begin
		case cstate is
			when reset =>
				nx_coord <= 320;
				ny_coord <= 240;
				ncount <= 0;
				if RST = '1' then
					nstate <= reset;
				else
					nstate <= waiting_for_drop;
				end if;
				
			when waiting_for_drop =>
				ncount <= 0;
				nx_coord <= x_coord;
				ny_coord <= y_coord;
				if DROP_BALL = '1' then
					nstate <= ball_drop;
				else
					nstate <= waiting_for_drop;
				end if;
				
			when ball_drop =>
				x_var := 0;
				y_var := 1;
				ncount <= count + 1;
				if count = 250000 then
					nx_coord <= x_coord + x_var;
					ny_coord <= y_coord + y_var;
					ncount <= 0;
				else
					nx_coord <= x_coord;
					ny_coord <= y_coord;
				end if;
				
				if y_coord = 474 then
					nstate <= move_left_up;
					ncount <= 0;
				else
					nstate <= ball_drop;
				end if;
				
			when move_left_up =>
				x_var := -1;
				y_var := -1;
				ncount <= count + 1;
				if count = 250000 then
					nx_coord <= x_coord + x_var;
					ny_coord <= y_coord + y_var;
					ncount <= 0;
				else
					nx_coord <= x_coord;
					ny_coord <= y_coord;
				end if;
				
				if x_coord = 7 AND y_coord /= 7 then
					nstate <= move_right_up;
					ncount <= 0;
				elsif y_coord = 7 AND x_coord /= 7 then
					nstate <= move_left_down;
					ncount <= 0;
				elsif x_coord = 7 AND y_coord = 7 then
					nstate <= move_right_down;
					ncount <= 0;
				else
					nstate <= move_left_up;
				end if;
				
			when move_left_down =>
				x_var := -1;
				y_var := 1;
				ncount <= count + 1;
				if count = 250000 then
					nx_coord <= x_coord + x_var;
					ny_coord <= y_coord + y_var;
					ncount <= 0;
				else
					nx_coord <= x_coord;
					ny_coord <= y_coord;
				end if;
				
				if x_coord = 6 AND y_coord /= 474 then
					nstate <= move_right_down;
					ncount <= 0;
				elsif y_coord = 474 AND x_coord /= 6 then
					nstate <= move_left_up;
					ncount <= 0;
				elsif x_coord = 6 AND y_coord = 474 then
					nstate <= move_left_up;
					ncount <= 0;
				else
					nstate <= move_left_down;
				end if;
			
			when move_right_down =>
				x_var := 1;
				y_var := 1;
				ncount <= count + 1;
				if count = 250000 then
					nx_coord <= x_coord + x_var;
					ny_coord <= y_coord + y_var;
					ncount <= 0;
				else
					nx_coord <= x_coord;
					ny_coord <= y_coord;
				end if;
				
				if x_coord = 634 AND y_coord /= 474 then
					nstate <= move_left_down;
					ncount <= 0;
				elsif y_coord = 474 AND x_coord /= 634 then
					nstate <= move_right_up;
					ncount <= 0;
				elsif x_coord = 634 AND y_coord = 474 then
					nstate <= move_left_up;
					ncount <= 0;
				else
					nstate <= move_right_down;
				end if;
			
			when move_right_up =>
				x_var := 1;
				y_var := -1;
				ncount <= count + 1;
				if count = 250000 then
					nx_coord <= x_coord + x_var;
					ny_coord <= y_coord + y_var;
					ncount <= 0;
				else
					nx_coord <= x_coord;
					ny_coord <= y_coord;
				end if;
				
				if x_coord = 634 AND y_coord /= 6 then
					nstate <= move_left_up;
					ncount <= 0;
				elsif y_coord = 6 AND x_coord /= 634 then
					nstate <= move_right_down;
					ncount <= 0;
				elsif x_coord = 634 AND y_coord = 6 then
					nstate <= move_left_down;
					ncount <= 0;
				else
					nstate <= move_right_up;
				end if;
				
						
			when others =>
				nx_coord <= x_coord;
				ny_coord <= y_coord;
				ncount <= count;
				nstate <= reset;

		end case;
	end process;


end architecture;